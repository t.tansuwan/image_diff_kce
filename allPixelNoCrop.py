import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
import glob
import pandas
import argparse
import time
import csv

def build_filters():
    filters = []
    #tesing phrase filter - reduce
    for ksize in range(9, 19, 5):
        for theta in np.arange(45, 225, 45):
            for sigma in range(2,6,2):
                kern = cv2.getGaborKernel((ksize, ksize), sigma, theta, 5.0, 0.5, 0, ktype=cv2.CV_32F)
                kern /= 1.5*kern.sum()
                filters.append(kern)
    return filters

def showFilter(f):
    #create figure
    fig = plt.figure(figsize=(60,60))
    #show each filter
    for k,im in enumerate(f):
        ax = fig.add_subplot(20, 20, k + 1)
        ax.imshow(im)
    plt.savefig('filter_result.png')
 
def process(images, f):
    accum = np.zeros_like(images)
    for kern in f:
        fimg = cv2.filter2D(images, cv2.CV_8UC3, kern)
        np.maximum(accum, fimg, accum)
    return accum

def importImage(img):
    print(img)
    img = cv2.imread(img, 0)

    return img

def separatePixel(output_master, output_defect):
    master_shape = output_master.shape
    master_pixel = []
    defect_pixel = []

    for r in range(master_shape[1]): #pixel row
        for c in range(master_shape[2]): #pixel column
            temp_master = []
            temp_defect = []
            for fi in range(master_shape[0]): #total number of filter using
                m = output_master[fi][r][c]
                d = output_defect[fi][r][c]
                temp_master.append(m)
                temp_defect.append(d)
            #save
            master_pixel.append(temp_master)
            defect_pixel.append(temp_defect)
    return(master_pixel, defect_pixel)

def computeCorr(master_pixel, defect_pixel):
    corr_result = []
    for i in range(len(master_pixel)):
        corr = np.corrcoef(master_pixel[i], defect_pixel[i])
        corr = corr[1,0]
        corr_result.append(corr)
    return corr_result

def thresholdCal(threshold, corr_result):
    if corr_result > threshold:
        return 'False'
    else:
        return 'True'

def findingImage(path, boardNo, defectNo, channel):
    path_ = path + '/bNo{0:03}_dNo{1:03}___{2}*'.format(boardNo, defectNo, channel)
    filenames = glob.glob(path_)
    return filenames[0]

if __name__ == '__main__':
    #parser
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--master_path',
        dest='master_path',
        required=True,
        help='Input master pictures path')
    parser.add_argument(
        '--defect_path', 
        dest='defect_path',
        required=True,
        help='Input defect pictures path')
    args = parser.parse_args()
    #list of defect
    defect_list = pandas.read_csv('ngList.csv')

    #read images from folderB
    masterPath = args.master_path
    defectPath = args.defect_path

    #Create filter
    filters = build_filters()
    f = np.asarray(filters)
    #Saving filter
    showFilter(f)
    print('created filter')
    

    #declare correlation threshold
    #>0.7 = Not defect <0.7 = Defect
    threshold = 0.7
    filename = 'allMargin05result_' + str(threshold) + '.csv'
    #create csv file
    with open(filename, 'w', newline='') as output:
        label = ['BoardNo', 'DefectNo', 'min_corr_R', 'min_corr_G', 'min_corr_B', 'min_corr_mean', 'prediction', 'time']
        writer = csv.DictWriter(output, fieldnames=label)
        writer.writeheader()
        #boardNo
        #going through all defect position in the board
        for d_position in range(len(defect_list)):
            print('start timer')
            start = time.time()
            
            #collect boardNo from 
            boardNo = defect_list['boardNo'][d_position]
            defectNo = defect_list['defectNo'][d_position]
            #crop master + defect image with the x1, x2, y1 and y2 from the provided csv
            crop_master_r = importImage(findingImage(masterPath, boardNo, defectNo, 'R'))
            crop_master_g = importImage(findingImage(masterPath, boardNo, defectNo, 'G'))
            crop_master_b = importImage(findingImage(masterPath, boardNo, defectNo, 'B'))
            crop_master = [crop_master_r, crop_master_g, crop_master_b]
            print('complete master')

            crop_defect_r = importImage(findingImage(defectPath, boardNo, defectNo, 'R'))
            crop_defect_g = importImage(findingImage(defectPath, boardNo, defectNo, 'G'))
            crop_defect_b = importImage(findingImage(defectPath, boardNo, defectNo, 'B'))
            crop_defect = [crop_defect_r, crop_defect_g, crop_defect_b]
            print('complete defect')

            all_corr_result = []
            for channel in range(len(crop_master)):
                master_pixel = []
                defect_pixel = []

                master_separate = []
                defect_separate = []
                print('applying filter to', channel)
                for fi in range(len(filters)):
                    #convolving gabor filter
                    res_master = process(crop_master[channel], filters[fi])
                    res_defect = process(crop_defect[channel], filters[fi])
                    #append the result of each filter in
                    master_pixel.append(res_master)
                    defect_pixel.append(res_defect)
                master_separate, defect_separate = separatePixel(np.asarray(master_pixel), np.asarray(defect_pixel))
                corr_result = computeCorr(master_separate, defect_separate)

                min_result = np.nanmin(corr_result)
                all_corr_result.append(min_result)
                print('corr complete')
            
            #find the average correlation result of three channel (R,G,B)
            mean_corr = np.mean(all_corr_result)
            prediction = thresholdCal(threshold, mean_corr)

            end = time.time()
            result_time = end - start

            info = {'BoardNo': boardNo, 'DefectNo': defectNo, 'min_corr_R':all_corr_result[0], 'min_corr_G':all_corr_result[1], 'min_corr_B':all_corr_result[2], 'min_corr_mean':mean_corr, 'prediction':prediction, 'time':result_time}
            writer.writerow(info)
            #if you want to loop only once, uncomment below
            #exit()
                

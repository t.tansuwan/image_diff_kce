import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
import glob
import pandas
import argparse
import time
import csv

def build_filters():
    filters = []
    #tesing phrase filter - reduce
    for ksize in range(9, 19, 5):
        for theta in np.arange(45, 225, 45):
            for sigma in range(2,6,2):
                kern = cv2.getGaborKernel((ksize, ksize), sigma, theta, 5.0, 0.5, 0, ktype=cv2.CV_32F)
                kern /= 1.5*kern.sum()
                filters.append(kern)
    return filters
 
def process(images, f):
    accum = np.zeros_like(images)
    for kern in f:
        fimg = cv2.filter2D(images, cv2.CV_8UC3, kern)
        np.maximum(accum, fimg, accum)
    return accum

def readPicture(path):
    #read images from the path
    filenames = glob.glob(path + "/*.jpg")
    sort_names = sorted(filenames, key = lambda name: int((name.split('_')[3]).split('.')[0]))
    return(sort_names)

def showFilter(f):
    #create figure
    fig = plt.figure(figsize=(60,60))
    #show each filter
    for k,im in enumerate(f):
        ax = fig.add_subplot(20, 20, k + 1)
        ax.imshow(im)
    plt.savefig('filter_result.png')

def showConvolvedOutput(result, name):
    fig = plt.figure(figsize=(100,100))
    for k, img in enumerate(result):
        figsize = 10
        ax1 = fig.add_subplot(figsize,figsize, k + 1)
        ax1.imshow(img)
    plt.tight_layout(pad=1.5)
    plt.savefig(name + '-result.png')

def separatePixel(master_shape, output_master, output_defect):
    master_pixel = []
    defect_pixel = []

    for r in range(master_shape[1]): #pixel row
        for c in range(master_shape[2]): #pixel column
            temp_master = []
            temp_defect = []
            for fi in range(master_shape[0]): #total number of filter using
                # print('filter', fi)
                m = output_master[fi][r][c]
                d = output_defect[fi][r][c]
                temp_master.append(m)
                temp_defect.append(d)
            #save
            master_pixel.append(temp_master)
            defect_pixel.append(temp_defect)
    return(master_pixel, defect_pixel)

def computeCorr(master_pixel, defect_pixel):
    corr = np.corrcoef(master_pixel, defect_pixel)
    corr_result = corr[1,0]
    return(corr_result)

def cropImg(img, x1, x2, y1, y2):
    cropimg = img[y1:y2 + 1, x1:x2 + 1]
    return cropimg

def thresholdCal(threshold, corr_result):
    if corr_result > threshold:
        return 'False'
    else:
        return 'True'

def checkXY(x1, x2, y1, y2):
    if x1 == x2:
        temp = y2 - y1
        x2 += temp
    if y1 == y2:
        temp = x2 - x1
        y2 += temp
    return x1, x2, y1, y2


if __name__ == '__main__':
    #list of defect
    defect_list = pandas.read_csv('defectlist.csv')

    #read images from folder
    master_r = '../B_master/KIBANCUR_R.jpg'
    master_b = '../B_master/KIBANCUR_B.jpg'
    master_g = '../B_master/KIBANCUR_G.jpg'
    
    defect_r = '../B_defect/R'
    defect_g = '../B_defect/G'
    defect_b = '../B_defect/B'

    #list of images
    defect_img_r = readPicture(defect_r)
    defect_img_g = readPicture(defect_g)
    defect_img_b = readPicture(defect_b)
    #read master pic
    master_r = cv2.imread(master_r, 0)
    master_g = cv2.imread(master_g, 0)
    master_b = cv2.imread(master_b, 0)

    #Create filter
    filters = build_filters()
    f = np.asarray(filters)
    #Saving filter
    showFilter(f)
    
    #count defects within each board
    count_boardD = defect_list.Defect_BoardNo.value_counts()
    count_boardD = count_boardD.sort_index()

    #declare correlation threshold
    #>0.7 = Not defect <0.7 = Defect
    threshold = 0.7
    filename = 'result_' + str(threshold) + '.csv'
    defect_count = 0
    #create csv file
    with open(filename, 'w', newline='') as output:
        label = ['BoardNo', 'DefectNo', 'corr_R', 'corr_G', 'corr_B', 'corr_mean', 'label', 'prediction', 'time']
        writer = csv.DictWriter(output, fieldnames=label)
        writer.writeheader()
        #boardNo
        for pic in range(len(defect_img_r)):
            print('pic', defect_img_r[pic], defect_img_g[pic], defect_img_b[pic])
            #read the board image from different channel
            img_r = cv2.imread(defect_img_r[pic], 0)
            img_b = cv2.imread(defect_img_b[pic], 0)
            img_g = cv2.imread(defect_img_g[pic], 0)

            #number of defect within that board
            count_defect = count_boardD[pic + 1]
            
            #going through all defect position in the board
            for d_position in range(count_defect):
                start = time.time()
                print('d_position', d_position)
                
                x1 = defect_list['Defect_X1'][defect_count]
                x2 = defect_list['Defect_X2'][defect_count]
                y1 = defect_list['Defect_Y1'][defect_count]
                y2 = defect_list['Defect_Y2'][defect_count]
                #check if same axis coordinate are the same
                x1, x2, y1, y2 = checkXY(x1, x2, y1, y2)
                
                #crop master + defect image with the x1, x2, y1 and y2 from the provided csv
                crop_master_r = cropImg(master_r, x1, x2, y1, y2)
                crop_master_g = cropImg(master_g, x1, x2, y1, y2)
                crop_master_b = cropImg(master_b, x1, x2, y1, y2)
                crop_master = [crop_master_r, crop_master_g, crop_master_b]

                crop_defect_r = cropImg(img_r, x1, x2, y1, y2)
                crop_defect_g = cropImg(img_g, x1, x2, y1, y2)
                crop_defect_b = cropImg(img_b, x1, x2, y1, y2)
                crop_defect = [crop_defect_r, crop_defect_g, crop_defect_b]

                x = defect_list['Defect_X'][defect_count] - x1
                y = defect_list['Defect_Y'][defect_count] - y1
                print(defect_list['Defect_X'][defect_count])
                print('x', x, 'y', y)

                #store (x, y) result
                #1 = R, 2 = G, 3 = B
                master_pixel = {1:[], 2:[], 3:[]}
                defect_pixel = {1:[], 2:[], 3:[]}
                all_corr_result = []
                for channel in range(3):
                    for fi in range(len(filters)):
                        #convolving gabor filter
                        res_master = process(crop_master[channel], filters[fi])
                        res_defect = process(crop_defect[channel], filters[fi])
                        #extracted pixel append into its channel
                        #append the result of each filter in
                        master_pixel[channel + 1].append(res_master[y][x])
                        defect_pixel[channel + 1].append(res_defect[y][x])
                    # print(master_pixel[channel + 1])
                    corr_result = computeCorr(master_pixel[channel + 1], defect_pixel[channel + 1])
                    print(channel, corr_result)
                    all_corr_result.append(corr_result)
                
                mean_corr = np.mean(all_corr_result)
                label = defect_list['FALSE_ALARM'][d_position]
                prediction = thresholdCal(threshold, mean_corr)

                #update to match up with csv defect row
                defect_count += 1

                end = time.time()
                result_time = end - start

                info = {'BoardNo':pic + 1, 'DefectNo': d_position + 1, 'corr_R':all_corr_result[0], 'corr_G':all_corr_result[1], 'corr_B':all_corr_result[2], 'corr_mean':mean_corr, 'label':label, 'prediction':prediction, 'time':result_time}
                writer.writerow(info)
                
                

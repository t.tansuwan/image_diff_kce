import cv2
import numpy as np
import math
import matplotlib.pyplot as plt
import scipy.stats as stat
import glob

def build_filters():
    filters = []
    # ksize = 9
    # for ksize in range(9, 19, 5):
    #     #B_3_4 45-180
    #     for theta in np.arange(0, 225, 45):
    #         for sigma in range(2,8,2): #B_3_4 2-6
    #             kern = cv2.getGaborKernel((ksize, ksize), sigma, theta, 5.0, 0.5, 0, ktype=cv2.CV_32F)
    #             kern /= 1.5*kern.sum()
    #             filters.append(kern)
    #tesing phrase filter - reduce
    for ksize in range(9, 19, 5):
        #B_3_4 45-180
        for theta in np.arange(45, 225, 45):
            for sigma in range(2,6,2): #B_3_4 2-6
                kern = cv2.getGaborKernel((ksize, ksize), sigma, theta, 5.0, 0.5, 0, ktype=cv2.CV_32F)
                kern /= 1.5*kern.sum()
                filters.append(kern)
    return filters
 
def process(images, f):
    accum = np.zeros_like(images)
    for kern in f:
        fimg = cv2.filter2D(images, cv2.CV_8UC3, kern)
        np.maximum(accum, fimg, accum)
    return accum

def readPicture(path):
    #read images from the path
    filenames = glob.glob(path + "/*.jpg")
    filenames.sort()
    images = [cv2.imread(img, 0) for img in filenames]

    ksize = 9
    #applying gaussian
    # for img in images:
    #     img = cv2.GaussianBlur(img, (ksize,ksize), 0)
    return images

def showFilter(f):
    #create figure
    fig = plt.figure(figsize=(60,60))
    #show each filter
    for k,im in enumerate(f):
        ax = fig.add_subplot(20, 20, k + 1)
        ax.imshow(im)
    plt.savefig('filter_result.png')

def showConvolvedOutput(result, name):
    fig = plt.figure(figsize=(100,100))
    for k, img in enumerate(result):
        figsize = 10
        ax1 = fig.add_subplot(figsize,figsize, k + 1)
        ax1.imshow(img)
    plt.tight_layout(pad=1.5)
    plt.savefig(name + '-result.png')

if __name__ == '__main__':
    #read images from folder
    d_name = '../defect/B/B_NG1_1.jpg'
    m_name = '../master/B/B_1_1.jpg'
    img_master = cv2.imread(m_name, 0)
    img_defect = cv2.imread(d_name, 0)
    img_master = cv2.GaussianBlur(img_master, (5,5),0)
    img_defect = cv2.GaussianBlur(img_defect, (5,5),0)
    master_shape = img_master.shape
    master_row = int(master_shape[0]/3)
    master_col = int(master_shape[1]/3)

    defect_shape = img_defect.shape
    defect_row = int(defect_shape[0]/3)
    defect_col = int(defect_shape[1]/3)

    img_master = img_master[master_row:(master_shape[0] + 1) - master_row, master_col:(master_shape[1] + 1) - master_col]
    img_defect = img_defect[defect_row:(defect_shape[0] + 1) - defect_row, defect_col:(defect_shape[1] + 1) - defect_col]


    cv2.imshow('master', img_master)
    cv2.imshow('defect', img_defect)
    cv2.waitKey(0)


    #Create filter
    filters = build_filters()
    f = np.asarray(filters)
    
    #Process with different filter
    res_master = []
    res_defect = []
    for i in range(len(filters)):
        res1 = process(img_master, filters[i])
        res2 = process(img_defect, filters[i])
        res_master.append(res1)
        res_defect.append(res2)

    #convert to np array
    output_master = np.asarray(res_master)
    output_defect = np.asarray(res_defect)

    master_shape = output_master.shape
    print(master_shape)
    master_pixel = []
    defect_pixel = []

    for r in range(master_shape[1]): #pixel row
        for c in range(master_shape[2]): #pixel column
            temp_master = []
            temp_defect = []
            for fi in range(master_shape[0]): #total number of filter using
                # print('filter', fi)
                m = output_master[fi][r][c]
                d = output_defect[fi][r][c]
                temp_master.append(m)
                temp_defect.append(d)
            #save
            master_pixel.append(temp_master)
            defect_pixel.append(temp_defect)
    
    print(master_pixel[0])
    #test correlation
    corr_result = []
    for i in range(len(master_pixel)):
        corr = np.corrcoef(master_pixel[i], defect_pixel[i])
        corr = corr[1,0]
        corr_result.append(corr)
    print(corr_result[0])
    corr_resut = np.asarray(corr_result)
    
    print('mean', np.mean(corr_result))
    print('min', np.amin(corr_result))
    print('max', np.amax(corr_result))
